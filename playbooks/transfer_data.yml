---
- name: Copy application data from one server to another
  vars_files:
    - ../environment/group_vars/all/all.yml
  hosts: all
  gather_facts: False
  tasks:
    - name: Generate SSH keys on old server
      block:
        - name: Generate ssh keypair
          shell: ssh-keygen -t ed25519 -f /root/.ssh/id_ed25519 -q -N ""
          args:
            creates: /root/.ssh/id_ed25519

        - name: Get content of public key file
          shell: cat /root/.ssh/id_ed25519.pub
          register: compositional_root_public_key

      when: "'old' in group_names"

    - name: Setup new server to receive data
      block:
        - name: Make sure directories' parent directories are present
          file:
            path: "{{ item['path'] }}"
            state: directory
            recurse: yes
            owner: "{{ item['id'] }}"
            group: "{{ item['id'] }}"
          loop:
            - {'id': 'root', 'path': '/srv'}
            - {'id': 'root', 'path': '/var/lib/docker/volumes'}
            - {'id': '999', 'path': '/srv/local/database_mysql'}

        - name: Add public key of root on old server to root on new server
          authorized_key:
            user: root
            state: present
            key: "{{ hostvars[groups['old'][0]]['compositional_root_public_key']['stdout'] }}"

      when: "'new' in group_names"

    - name: Finish Sync on old server
      block:
        - name: rsync directories over
          shell: rsync -ave "ssh -i /root/.ssh/id_ed25519 -o StrictHostKeyChecking=no" {{ item.src }} root@{{ groups['new'][0] }}:{{ item.dest }}
          loop:
            - {'src': '/etc/letsencrypt/', 'dest': '/etc/letsencrypt'}
            - {'src': '--exclude local/database_mysql /srv/', 'dest': '/srv'}
            - {'src': '/srv/local/database_mysql/backups', 'dest': '/srv/local/database_mysql'}

        - name: Remove SSH keypair for root on old server
          file:
            path: "/root/.ssh/id_ed25519{{ item }}"
            state: absent
          loop:
            - '.pub'
            - ''

      when: "'old' in group_names"

    - name: Remove public key of root on old server for root on new server
      authorized_key:
        user: root
        state: absent
        key: "{{ hostvars[groups['old'][0]]['compositional_root_public_key']['stdout'] }}"
      when: "'new' in group_names"

